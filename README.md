# McFalls Budget -> Everyman Budget Migrator

Migrates a McFalls Budget database to an Everyman Budget. This is really migrating from version 1.0 to 2.0.

I don't expect anyone other than me should actually need to use this.

## Use

```
usage: migrate.py [-h] --db-path DB_PATH --db-name DB_NAME [--db-host DB_HOST] --db-username DB_USERNAME --db-password DB_PASSWORD [--log-file LOG_FILE]

Migrates data from EMB v1.0 to v2.0

optional arguments:
  -h, --help            show this help message and exit
  --db-path DB_PATH     Path to the EMB1.0 database file
  --db-name DB_NAME     Name of the EMB2.0 database
  --db-host DB_HOST     Host the EMB2.0 database is on
  --db-username DB_USERNAME
                        Username to connect to the EMB2.0 database with
  --db-password DB_PASSWORD
                        Password to connect to the EMB2.0 database with
  --log-file LOG_FILE   File to log to

```

## Comments

I would like to have two different copies of the repository, one at the 1.0 commit and one at 2.0.
It doesn't look like that's possible, but I needed to make some small changes to 1.0 to make it more usable,
so copy/pasting it was probably better anyway.
