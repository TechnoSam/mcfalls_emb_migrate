import argparse
from decimal import Decimal
import logging
import os
import sys

import colorlog
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

import mcfalls_budget.budget as mb

from everyman_budget.database import action
from everyman_budget.database.schema import Account, Party, Transaction
from everyman_budget.initialize import is_initialized, initialize
import everyman_budget.config as embc

logger = logging.getLogger()


def main(path: str, db_name: str, host: str, username: str, password: str):
    os.environ['BUDGET_DB_NAME'] = db_name
    os.environ['BUDGET_DB_HOST'] = host
    os.environ['BUDGET_DB_USERNAME'] = username
    os.environ['BUDGET_DB_PASSWORD'] = password

    emb_config = embc.Config()
    engine = create_engine(emb_config.url)
    Session = sessionmaker(engine)

    account_manager = mb.AccountManager(db_name=path)
    accounts = [x[0] for x in account_manager.list_accounts()]
    # print(accounts)
    txs = account_manager.list_history_filter()
    # print('\n'.join([x.as_string() for x in txs]))

    if not is_initialized():
        initialize(alembic_dir='everyman_budget')
    else:
        logger.critical('Cannot migrate to an existing database. This is to protect against double migrating.')
        exit(1)

    with Session() as session:
        for account in accounts:
            logger.info('Adding account {name}'.format(name=account))
            action.add_account(session, account)
        tx_count = 0
        for tx in txs:
            logger.info('Adding tx {count}'.format(count=tx_count))
            (from_account, to_account) = tx.get_accounts()
            if from_account == '':
                from_account = None
            else:
                matches = action.find_account_by_name(session, from_account)
                for match in matches:
                    if from_account == match.name:
                        from_account = match
                        break
            if to_account == '':
                to_account = None
            else:
                matches = action.find_account_by_name(session, to_account)
                for match in matches:
                    if to_account == match.name:
                        to_account = match
                        break

            if from_account is not None and to_account is not None:
                action.transfer(session, to_account, from_account, Decimal(str(tx.get_charge())), tx.get_date(),
                                notes=tx.get_notes())
            else:
                account = from_account if from_account is not None else to_account
                charge = Decimal(str(tx.get_charge()))
                charge = charge * (-1 if from_account is not None else 1)
                action.add_transaction(session, account, charge, tx.get_date(), notes=tx.get_notes())
            tx_count += 1

        session.commit()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Migrates data from EMB v1.0 to v2.0')
    parser.add_argument('--db-path', required=True, help='Path to the EMB1.0 database file')
    parser.add_argument('--db-name', required=True, help='Name of the EMB2.0 database')
    parser.add_argument('--db-host', default='127.0.0.1', help='Host the EMB2.0 database is on')
    parser.add_argument('--db-username', required=True, help='Username to connect to the EMB2.0 database with')
    parser.add_argument('--db-password', required=True, help='Password to connect to the EMB2.0 database with')
    parser.add_argument('--log-file', default='mcfalls_emb_migrate.log', help='File to log to')

    args = parser.parse_args()

    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s | %(levelname)-8s | %(message)s | %(name)s:%(lineno)d')
    colored_formatter = colorlog.ColoredFormatter(
        '%(asctime)s | %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s | %(name)s:%(lineno)d',
        log_colors={
            'DEBUG': 'bold_cyan',
            'INFO': 'bold_green',
            'WARNING': 'bold_yellow',
            'ERROR': 'bold_red',
            'CRITICAL': 'bold_red,bg_bold_white',
        },
    )

    stream_handler = logging.StreamHandler()
    stream_handler.setStream(sys.stderr)  # Logging is diagnostic info and should go to stderr
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(colored_formatter if sys.stderr.isatty() else formatter)
    logger.addHandler(stream_handler)

    file_handler = logging.FileHandler(args.log_file)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    logging.getLogger('sqlalchemy.engine').setLevel(logging.WARNING)
    logging.getLogger('alembic').setLevel(logging.WARNING)
    logging.getLogger('werkzeug').setLevel(logging.WARNING)
    logging.getLogger('everyman_budget.database.action').setLevel(logging.INFO)

    main(args.db_path, args.db_name, args.db_host, args.db_username, args.db_password)
